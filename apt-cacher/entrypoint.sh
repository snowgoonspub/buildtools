#!/bin/sh

CACHEDIR=/var/cache/apt-cacher-ng
LOGDIR=/var/log/apt-cacher-ng

chmod 777 ${CACHEDIR}
/etc/init.d/apt-cacher-ng start
tail -f ${LOGDIR}/*
