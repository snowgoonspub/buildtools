BRANCH=${CI_COMMIT_REF_NAME:-`git branch --show-current`}
BRANCHTAG=${BRANCH/\//-}

if [ "X${BRANCHTAG}" = Xmaster ]; then
  BRANCHTAG=latest
fi
if [ "X${BRANCHTAG}" = Xmain ]; then
  BRANCHTAG=latest
fi


echo "Building APT Cacher"
( cd apt-cacher
  docker build -t snowgoons/apt-cacher:${BRANCHTAG} .
  docker push snowgoons/apt-cacher
)
